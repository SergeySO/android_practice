package com.example.practike_rest.users.data;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;

import com.example.practike_rest.users.domain.PostCallback;
import com.example.practike_rest.users.domain.model.*;

public class ExampleOKHttpRepository extends BaseExampleRepository {
    private static final String TAG = ExampleOKHttpRepository.class.getName();

    @NotNull
    private final OkHttpClient mHttpClient;

    public ExampleOKHttpRepository() {
        this.mHttpClient = new OkHttpClient.Builder()
                .readTimeout(3, TimeUnit.SECONDS)
                .writeTimeout(3, TimeUnit.SECONDS)
                .addNetworkInterceptor(new HttpLoggingInterceptor())
                .build();
    }

    @Override
    public void postsAdd(Post post, PostCallback callback) {
        Request request = new Request.Builder()
                .url(HttpUrl.get(REQUEST_API))
                .addHeader("Content-Type", "application/json")
                .post(RequestBody.create(getRequestBody(post).getBytes()))
                .build();
        handleResponse(request, callback);
    }

    @Override
    public void postsList(PostCallback callback) {
        Request request = new Request.Builder()
                .url(HttpUrl.get(REQUEST_API))
                .build();
        handleResponse(request, callback);
    }

    private void handleResponse(Request request, PostCallback callback) {
        mHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.e(TAG, "Request failure: ", e);
                callback.response(e.toString());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    ResponseBody body = response.body();
                    callback.response(body != null ? body.string() : "No content");
                } else {
                    callback.response("Response code:" + response.code());
                }
            }
        });
    }
}
