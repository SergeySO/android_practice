package com.example.practike_rest.users.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.practike_rest.R;
import com.example.practike_rest.users.data.ExampleOKHttpRepository;
import com.example.practike_rest.users.domain.ExampleInteractor;

import java.lang.ref.WeakReference;

import com.example.practike_rest.users.domain.PostCallback;
import com.example.practike_rest.users.domain.model.*;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    private final ExampleInteractor integrator = new ExampleInteractor(new ExampleOKHttpRepository());
    private MyPostCallback mPostCallback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView mResponseView = findViewById(R.id.tvResponse);
        Button btnPostsList = findViewById(R.id.btnPostsList);
        Button btnPostsAdd = findViewById(R.id.btnPostsAdd);
        mPostCallback = new MyPostCallback(new WeakReference<>(mResponseView));

        btnPostsList.setOnClickListener(v -> {
            integrator.postsList(mPostCallback);
        });
        btnPostsAdd.setOnClickListener(v ->
                integrator.postsAdd(
                        new Post(100, "title", "body"),
                        mPostCallback));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPostCallback.cleanView();
    }

    private static class MyPostCallback implements PostCallback {
        private final WeakReference<TextView> mResponseView;

        private MyPostCallback(WeakReference<TextView> mResponseView) {
            this.mResponseView = mResponseView;
        }

        @Override
        public void response(String text) {
            TextView textView = mResponseView.get();

            if (textView != null) {
                textView.post(() -> textView.setText(text));
            }
        }

        private void cleanView() {
            mResponseView.clear();
        }
    }
}