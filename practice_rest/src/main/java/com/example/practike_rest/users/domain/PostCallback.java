package com.example.practike_rest.users.domain;

public interface PostCallback {
    void response(String text);
}
