package com.example.practike_rest.users.data;

import com.example.practike_rest.users.domain.ExampleRepository;
import com.example.practike_rest.users.domain.model.Post;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

public abstract class BaseExampleRepository implements ExampleRepository {
    public String REQUEST_API = "https://jsonplaceholder.typicode.com/posts";

    public String getRequestBody(Post post) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<Post> adapter = moshi.adapter(Post.class);
        return adapter.toJson(post);
    }
}
