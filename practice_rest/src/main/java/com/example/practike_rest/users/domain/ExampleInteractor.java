package com.example.practike_rest.users.domain;

import com.example.practike_rest.users.domain.model.*;

public class ExampleInteractor {
    private final ExampleRepository mRepository;

    public ExampleInteractor(ExampleRepository mRepository) {
        this.mRepository = mRepository;
    }

    public void postsAdd(Post post, PostCallback callback) {
        mRepository.postsAdd(post, callback);
    }

    public void postsList(PostCallback callback) {
        mRepository.postsList(callback);
    }
}
