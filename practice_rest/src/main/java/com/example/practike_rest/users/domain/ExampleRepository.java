package com.example.practike_rest.users.domain;

import com.example.practike_rest.users.domain.model.*;

public interface ExampleRepository {
    void postsAdd(Post post, PostCallback callback);
    void postsList(PostCallback callback);
}
