package com.example.practike_rest.chucknorris.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.practike_rest.R;
import com.example.practike_rest.chucknorris.data.MyOKHttpRepository;
import com.example.practike_rest.chucknorris.domain.MyInteractor;
import com.example.practike_rest.chucknorris.domain.PostCallback;

import java.lang.ref.WeakReference;

public class RestExampleActivity extends AppCompatActivity {
    private final MyInteractor mInteractor = new MyInteractor(new MyOKHttpRepository());
    private MyCallback myCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_example);
        Button nextBtn = findViewById(R.id.nextBtn);
        TextView factTV = findViewById(R.id.factTv);
        myCallback = new MyCallback(new WeakReference<>(factTV));
        nextBtn.setOnClickListener(view -> mInteractor.post(myCallback));
    }

    @Override
    protected void onStop() {
        super.onStop();
        myCallback.cleanView();
    }

    private static class MyCallback implements PostCallback {
        private final WeakReference<TextView> mResponseView;

        private MyCallback(WeakReference<TextView> mResponseView) {
            this.mResponseView = mResponseView;
        }

        @Override
        public void response(String text) {
            TextView textView = mResponseView.get();

            if (textView != null) {
                textView.post(() -> textView.setText(text));
            }
        }

        private void cleanView() {
            mResponseView.clear();
        }
    }
}