package com.example.practike_rest.chucknorris.data;

import android.util.Log;

import com.example.practike_rest.chucknorris.domain.PostCallback;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;

public class MyOKHttpRepository extends MyBaseRepository {
    public static final String TAG = MyBaseRepository.class.getName();

    private final OkHttpClient mHttpClient;

    public MyOKHttpRepository() {
        this.mHttpClient = new OkHttpClient()
                .newBuilder()
                .writeTimeout(3, TimeUnit.SECONDS)
                .readTimeout(3, TimeUnit.SECONDS)
                .addNetworkInterceptor(new HttpLoggingInterceptor())
                .build();
    }

    @Override
    public void post(PostCallback callback) {
        Request request = new Request.Builder()
                .url(REQUEST_API)
                .build();
        handleResponse(request, callback);
    }

    private void handleResponse(Request request, PostCallback callback) {
        mHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.e(TAG, "Request failure: ", e);
                callback.response(e.toString());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    ResponseBody body = response.body();
                    String text;

                    if (body != null) {
                        text = getRootData(body.string()).getPhrase().getJoke();
                    } else {
                        text = "No context";
                    }

                    callback.response(text);
                } else {
                    callback.response("Response code: " + response.code());
                }
            }
        });
    }
}
