package com.example.practike_rest.chucknorris.domain;

public class MyInteractor {
    private final MyRepository myRepository;

    public MyInteractor(MyRepository myRepository) {
        this.myRepository = myRepository;
    }

    public void post(PostCallback callback) {
        myRepository.post(callback);
    }
}
