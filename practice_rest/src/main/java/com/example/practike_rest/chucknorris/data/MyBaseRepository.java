package com.example.practike_rest.chucknorris.data;

import com.example.practike_rest.chucknorris.domain.MyRepository;
import com.example.practike_rest.chucknorris.domain.model.Phrase;
import com.example.practike_rest.chucknorris.domain.model.Root;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.util.ArrayList;

public abstract class MyBaseRepository implements MyRepository {
    public final String REQUEST_API = "https://api.icndb.com/jokes/random";

    protected Root getRootData(String text) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<Root> adapter = moshi.adapter(Root.class);
        try {
            return adapter.fromJson(text);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Root("", new Phrase(0, "", new ArrayList<>()));
    }
}
