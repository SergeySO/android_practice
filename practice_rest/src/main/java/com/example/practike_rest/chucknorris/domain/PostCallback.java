package com.example.practike_rest.chucknorris.domain;

public interface PostCallback {
    void response(String text);
}
