package com.example.practike_rest.chucknorris.domain.model;

public class Root {
    private String type;
    private Phrase value;

    public Root(String type, Phrase value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Phrase getPhrase() {
        return value;
    }

    public void setPhrase(Phrase value) {
        this.value = value;
    }
}
