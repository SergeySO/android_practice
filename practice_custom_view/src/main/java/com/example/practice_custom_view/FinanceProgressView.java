package com.example.practice_custom_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;

public class FinanceProgressView extends View {
    private final String TAG = "FinanceProgressView";
    private final float START_TOP_ANGLE = -90f;
    private final float FULL_CIRCLE_ANGLE = 360f;

    @ColorInt
    private int color = 0;

    @ColorInt
    private int colorBackground = 0;

    private int progress = 0;
    private float strokeWidth = 0f;
    private float textSize = 0f;
    private int max = 0;
    private final RectF arcRect = new RectF();
    private final Rect textBoundsRect = new Rect();
    private final Paint backgroundArcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint foreignArcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public FinanceProgressView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
        backgroundArcPaint.setStyle(Paint.Style.STROKE);
        foreignArcPaint.setStyle(Paint.Style.STROKE);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FinanceProgressView,
                R.attr.financeProgressDefaultAttr,
                0
        );
        try {
            color = typedArray.getColor(R.styleable.FinanceProgressView_android_color, 0);
            colorBackground = typedArray.getColor(R.styleable.FinanceProgressView_android_colorBackground, 0);
            progress = typedArray.getInt(R.styleable.FinanceProgressView_android_progress, 0);
            strokeWidth = (float) typedArray.getDimensionPixelSize(R.styleable.FinanceProgressView_strokeWidth, 0);
            textSize = (float) typedArray.getDimensionPixelSize(R.styleable.FinanceProgressView_android_textSize, 0);
            max = typedArray.getInteger(R.styleable.FinanceProgressView_android_max, 0);
        } finally {
            typedArray.recycle();
        }

        updatePairs();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw called with: canvas" + canvas);
        updatePairs();
        canvas.drawArc(arcRect, 0f, FULL_CIRCLE_ANGLE, false, backgroundArcPaint);
        canvas.drawArc(arcRect, START_TOP_ANGLE, FULL_CIRCLE_ANGLE * progress / max, false, foreignArcPaint);
        drawText(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        Log.d(TAG, "onSizeChanged() called with: w = $w, h = $h, oldw = $oldw, oldh = $oldh");
        super.onSizeChanged(w, h, oldw, oldh);

        float size = Math.min(w, h) - strokeWidth / 2;
        arcRect.set(strokeWidth / 2 + getPaddingLeft(),
                strokeWidth / 2 + getPaddingTop(),
                size - getPaddingRight(),
                size - getPaddingBottom()
        );
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(TAG, "onMeasure: widthMeasureSpec =" +
                MeasureSpec.toString(widthMeasureSpec) + ", heightMeasureSpec = " +
                MeasureSpec.toString(heightMeasureSpec));
        String text = max + "%";
        textPaint.getTextBounds(text, 0, text.length(), textBoundsRect);

        int measuredWidth = (int) (strokeWidth * 2 + textBoundsRect.width() + getPaddingLeft() + getPaddingRight());
        int measuredHeight = (int) (strokeWidth * 2 + textBoundsRect.height() + getPaddingTop() + getPaddingBottom());
        int requestedWidth = Math.max(measuredWidth, getSuggestedMinimumWidth());
        int requestedHeight = Math.max(measuredHeight, getSuggestedMinimumWidth());
        int requestedSize = Math.max(requestedHeight, requestedWidth);

        setMeasuredDimension(
                resolveSizeAndState(requestedSize, widthMeasureSpec, 0),
                resolveSizeAndState(requestedSize, heightMeasureSpec, 0)
        );
    }

    //todo делать реализацию сохранения не обязательно т.к. в SeekBar сохраниние progress уже реализован под капотом
    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState mySate = new SavedState(superState);
        mySate.progress = this.progress;
        return mySate;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.progress = savedState.progress;
        requestLayout(); //or ....
        invalidate();// or .....
    }

    public void setColor(@ColorInt int color) {
        this.color = color;
        invalidate();
    }

    public void setProgress(int progress) {
        this.progress = progress;
        invalidate();
    }

    private void drawText(Canvas canvas) {
        String progressStr = progress + "%";
        textPaint.getTextBounds(progressStr, 0, progressStr.length(), textBoundsRect);
        canvas.drawText(progressStr,
                arcRect.width() / 2 + arcRect.left - textBoundsRect.width() / 2,
                arcRect.height() / 2 + arcRect.top + textBoundsRect.height() / 2,
                textPaint);
    }

    private void updatePairs() {
        foreignArcPaint.setColor(this.color);
        foreignArcPaint.setStrokeWidth(this.strokeWidth);
        backgroundArcPaint.setColor(this.colorBackground);
        backgroundArcPaint.setStrokeWidth(this.strokeWidth);
        textPaint.setTextSize(this.textSize);
        textPaint.setColor(this.color);
    }

    static class SavedState extends BaseSavedState {
        private int progress;

        public SavedState(Parcelable state) {
            super(state);
        }

        private SavedState(Parcel in) {
            super(in);
            progress = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(progress);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
