package com.example.complex_gestures.multitouch;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import com.example.complex_gestures.R;

public class MultiTouchTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_touch_test);

        initUI();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUI() {
        FingersTestView fingersTestView = findViewById(R.id.draw_view);
        TextView textView = findViewById(R.id.txt_log);
        fingersTestView.setOnTouchListener((v, event) -> {
            //index: XX, ID: XX, X: XX, Y: XX
            int pointerCount = event.getPointerCount();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < pointerCount; i++) {
                sb.append("Index: ").append(i);
                sb.append(", ID: ").append(event.getPointerId(i));
                sb.append(", X: ").append(event.getX(i));
                sb.append(", Y: ").append(event.getY(i));
                sb.append("\n");
            }

            textView.setText(sb.toString());
            return false;
        });
    }
}