package com.example.complex_gestures.multitouch;

import android.graphics.PointF;

public class Circle {
    private PointF mOrigin;
    private float radius;
    private int mColor;

    public Circle(PointF mOrigin, float radius, int mColor) {
        this.mOrigin = mOrigin;
        this.radius = radius;
        this.mColor = mColor;
    }

    public PointF getOrigin() {
        return mOrigin;
    }

    public void setOrigin(PointF mOrigin) {
        this.mOrigin = mOrigin;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int mColor) {
        this.mColor = mColor;
    }
}
