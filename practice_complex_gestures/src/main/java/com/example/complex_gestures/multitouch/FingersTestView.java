package com.example.complex_gestures.multitouch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import androidx.annotation.Nullable;

import java.util.Arrays;
import java.util.Stack;

import static android.view.MotionEvent.ACTION_CANCEL;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_POINTER_DOWN;
import static android.view.MotionEvent.ACTION_POINTER_UP;
import static android.view.MotionEvent.ACTION_UP;

public class FingersTestView extends View {
    private static final float CIRCLE_RADIUS = 100f;
    private static final float CIRCLE_STROKE_WIDTH = 30f;
    private final SparseArray<Circle> mCircles = new SparseArray<>();
    private final Stack<Integer> mColors = new Stack<>();
    private final Paint mPaint = new Paint();

    public FingersTestView(Context context) {
        this(context, null);
    }

    public FingersTestView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setupPaint();
        setupColors();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (int i = 0; i < mCircles.size(); i++) {
            Circle circle = mCircles.valueAt(i);
            mPaint.setColor(circle.getColor());
            canvas.drawCircle(circle.getOrigin().x, circle.getOrigin().y, circle.getRadius(), mPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();
        int pointerIndex = event.getActionIndex();
        int pointerId = event.getPointerId(pointerIndex);
        Circle currentCircle;

        switch (action) {
            case ACTION_DOWN:
            case ACTION_POINTER_DOWN:
                PointF currentPoint = new PointF(event.getX(), event.getY());
                currentCircle = new Circle(currentPoint, CIRCLE_RADIUS, getNextColor());
                mCircles.put(pointerId, currentCircle);
                break;
            case ACTION_MOVE:
                for (int i = 0; i < event.getPointerCount(); i++) {
                    int id = event.getPointerId(i);
                    currentCircle = mCircles.get(id);
                    PointF newPointF = new PointF(event.getX(i), event.getY(i));
                    currentCircle.setOrigin(newPointF);
                }

                break;
            case ACTION_POINTER_UP:
                currentCircle = mCircles.get(pointerId);
                returnColor(currentCircle.getColor());
                mCircles.delete(pointerId);
                break;
            case ACTION_UP:
            case ACTION_CANCEL:
                setupColors();
                mCircles.clear();
                break;
        }

        invalidate();
        return true;
    }

    private void setupPaint() {
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(CIRCLE_STROKE_WIDTH);
    }

    private int getNextColor() {
        Integer value = mColors.pop();

        if (value == null) {
            return Color.BLACK;
        } else {
            return value;
        }
    }

    private void returnColor(int color) {
        mColors.push(color);
    }

    private void setupColors() {
        mColors.clear();
        mColors.addAll(Arrays.asList(
                Color.RED,
                Color.GRAY,
                Color.BLUE,
                Color.LTGRAY,
                Color.YELLOW,
                Color.DKGRAY,
                Color.BLACK,
                Color.GREEN,
                Color.CYAN,
                Color.MAGENTA
        ));
    }
}
