package com.example.complex_gestures;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

import com.example.complex_gestures.R;

import static android.view.MotionEvent.ACTION_CANCEL;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_OUTSIDE;
import static android.view.MotionEvent.ACTION_POINTER_DOWN;
import static android.view.MotionEvent.ACTION_POINTER_UP;
import static android.view.MotionEvent.ACTION_UP;

public class MultiTouchExampleActivity extends AppCompatActivity {
    public static final String TAG = "SingleOrMulti";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_touch_example);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();
        String actionCode = "";

        switch (action) {
            case ACTION_DOWN:
                actionCode = "Down";
                break;
            case ACTION_MOVE:
                actionCode = "Move";
                break;
            case ACTION_UP:
                actionCode = "Up";
                break;
            case ACTION_POINTER_DOWN:
                actionCode = "Pointer down";
                break;
            case ACTION_POINTER_UP:
                actionCode = "Pointer up";
                break;
            case ACTION_OUTSIDE:
                actionCode = "Outside";
                break;
            case ACTION_CANCEL:
                actionCode = "Cancel";
                break;
        }

        Log.i(TAG, "The action is: " + actionCode);
        int index = event.getActionIndex();
        int xPos = -1;
        int yPos = -1;

        if (event.getPointerCount() > 1) {
            Log.i(TAG, "Multi-Touch event");
        } else {
            Log.i(TAG, "Single Touch event");
            return true;
        }

        xPos = (int) event.getX(index);
        yPos = (int) event.getY(index);

        Log.i(TAG, "xPosition: " + xPos + ", yPosition: " + yPos);
        return true;
    }
}