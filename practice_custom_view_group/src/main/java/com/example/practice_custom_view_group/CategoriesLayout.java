package com.example.practice_custom_view_group;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class CategoriesLayout extends ViewGroup {
    public CategoriesLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        Log.d(MyLayoutParams.TAG, "onLayout() called with: changed = [" +
                changed + "], l = [" + l + "], t = [" + t + "], r = [" + r + "], b = [" + b + "]");
        Log.d(MyLayoutParams.TAG, "onLayout() called with: width = [" +
                getWidth() + "], height = [" + getHeight() + "]");

        int offset = getPaddingLeft();//начало отсчета для следующего элемента (отступ для следующего элемента)
        int rightBound = r - l - getPaddingRight(); //правая граница нашей view

        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            MyLayoutParams lp = (MyLayoutParams) child.getLayoutParams();
            int childL = offset + lp.leftMargin;
            int childT = getPaddingTop() + lp.topMargin;
            int childR = childL + child.getMeasuredWidth();
            int childB = childT + child.getMeasuredHeight();

            if (childR > rightBound) {
                child.layout(0, 0, 0, 0);
            } else {
                child.layout(childL, childT, childR, childB);
            }

            offset = childR + lp.rightMargin;//сдвиг к правой границе
        }
    }

    @Override
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MyLayoutParams(getContext(), attrs);
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        if (p instanceof MarginLayoutParams) {
            return new MyLayoutParams((MarginLayoutParams) p);
        }

        return new MyLayoutParams(p);
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new MyLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof MyLayoutParams;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(MyLayoutParams.TAG, "onMeasure() called with: widthMeasureSpec = [" +
                MeasureSpec.toString(widthMeasureSpec) + "], heightMeasureSpec = [" +
                MeasureSpec.toString(heightMeasureSpec) + "]");

        int requestedWidth = 0;
        int requestedHeight = 0;
        int childMeasureState = 0;

        //нужно пройтись по всем child чтоб померить их, чтоб померить себя
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            MyLayoutParams lp = (MyLayoutParams) child.getLayoutParams();
            //учитывает родительские Specs, LayoutParams, Margins и т.д. и формирует MeasureSpecs для child и вызывает у них метод measure
            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);
            //далее используем результат, который намерился (берем его Measure и Margins)
            requestedWidth += child.getMeasuredWidth() + lp.rightMargin + lp.leftMargin;
            requestedHeight = Math.max(requestedHeight, child.getMeasuredHeight());

            childMeasureState = combineMeasuredStates(childMeasureState, child.getMeasuredState());
        }

        //подсчет padding (За это должен быть ответственный как раз на Layout)
        requestedHeight += getPaddingTop() + getPaddingBottom();
        requestedWidth += getPaddingLeft() + getPaddingRight();

        //подсчет ширины и высоты
        requestedWidth = Math.max(getSuggestedMinimumWidth(), requestedWidth);
        requestedHeight = Math.max(getSuggestedMinimumHeight(), requestedHeight);

        //возвращаем запрашиваемую ширину и высоту
        setMeasuredDimension(
                resolveSizeAndState(requestedWidth, widthMeasureSpec, childMeasureState),
                resolveSizeAndState(requestedHeight, heightMeasureSpec,
                        childMeasureState << MEASURED_HEIGHT_STATE_SHIFT));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(MyLayoutParams.TAG, "onTouchEvent() called with: event = [" + event + "]");
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.d(MyLayoutParams.TAG, "onInterceptTouchEvent() called with: ev = [" + ev + "]");
        return super.onInterceptTouchEvent(ev);
    }

    static class MyLayoutParams extends MarginLayoutParams {
        public static final String TAG = "MyLayoutParams";

        public MyLayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
        }

        public MyLayoutParams(MarginLayoutParams source) {
            super(source);
        }

        public MyLayoutParams(LayoutParams source) {
            super(source);
        }

        public MyLayoutParams(int width, int height) {
            super(width, height);
        }
    }
}
