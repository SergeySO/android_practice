package com.example.practice_animations.anims;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class FrameAnimationActivity extends AppCompatActivity {
    private AnimationDrawable frameAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_animation);

        ImageView imageView = findViewById(R.id.my_animation);
        frameAnimation = (AnimationDrawable) imageView.getDrawable();

        Button switchBtn = findViewById(R.id.switch_btn);
        switchBtn.setOnClickListener(v -> {
            if (frameAnimation.isRunning()) {
                frameAnimation.stop();
            } else {
                frameAnimation.start();
            }
        });
    }
}