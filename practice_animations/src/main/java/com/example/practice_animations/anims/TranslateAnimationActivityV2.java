package com.example.practice_animations.anims;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class TranslateAnimationActivityV2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translate_animation_v2);
        ImageView imageView = findViewById(R.id.translate_image);

        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0, -100 , 0);
        translateAnimation.setDuration(2500);
        translateAnimation.setFillAfter(true);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0, 0, 100 , 0);
        translateAnimation2.setDuration(2500);
        translateAnimation2.setFillAfter(true);
        translateAnimation2.setStartOffset(2500);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(translateAnimation2);
        imageView.startAnimation(animationSet);
    }
}