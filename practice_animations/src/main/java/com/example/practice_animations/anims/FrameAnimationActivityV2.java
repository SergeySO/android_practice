package com.example.practice_animations.anims;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.animation.AnimationSet;
import android.widget.Button;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class FrameAnimationActivityV2 extends AppCompatActivity {
    private AnimationDrawable frameAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_animation_v2);
        ImageView imageView = findViewById(R.id.my_animation);
        frameAnimation = new AnimationDrawable();

        for (int i = 0; i <=6; i++) {
            //todo в качестве примера норм, но так лучше не делать
            int id = getResources().getIdentifier(
                    "baseline_battery_charging_" + i, "drawable",
                    FrameAnimationActivityV2.this.getPackageName());
            Drawable drawable = getResources().getDrawable(id);
            frameAnimation.addFrame(drawable, 200);
            AnimatorSet animatorSet;
            AnimationSet animationSet;
        }

        imageView.setImageDrawable(frameAnimation);
        frameAnimation.start();

        Button switchBtn = findViewById(R.id.switch_btn);
        switchBtn.setOnClickListener(v -> {
            if (frameAnimation.isRunning()) {
                frameAnimation.stop();
            } else {
                frameAnimation.start();
            }
        });
    }
}