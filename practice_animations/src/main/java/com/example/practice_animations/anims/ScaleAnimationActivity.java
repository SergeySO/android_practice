package com.example.practice_animations.anims;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class ScaleAnimationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scale_animation);
        ImageView imageView = findViewById(R.id.scale_anim_view);
        Animation scaleAnimation = AnimationUtils.loadAnimation(this, R.anim.scale);
        imageView.startAnimation(scaleAnimation);
    }
}