package com.example.practice_animations.anims;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class FullAnimationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_animation);
        ImageView imageView = findViewById(R.id.full_image);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.full_animation);
        imageView.startAnimation(animation);
    }
}