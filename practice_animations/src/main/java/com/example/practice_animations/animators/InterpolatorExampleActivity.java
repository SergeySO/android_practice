package com.example.practice_animations.animators;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class InterpolatorExampleActivity extends AppCompatActivity {
    private ImageView imageView;
    private Animation animAccelerate;
    private Animation animDecelerate;
    private Animation animAccelerateDecelerate;
    private Animation animAnticipate;
    private Animation animAnticipateOvershoot;
    private Animation animOvershoot;
    private Animation animBounce;
    private Animation animCycle;
    private Animation animLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interpolator_example);
        imageView = findViewById(R.id.image_view);
        animAccelerate = AnimationUtils.loadAnimation(this, R.anim.accelerate);
        animDecelerate = AnimationUtils.loadAnimation(this, R.anim.decelerate);
        animAccelerateDecelerate = AnimationUtils.loadAnimation(this, R.anim.accelerate_decelerate);
        animAnticipate = AnimationUtils.loadAnimation(this, R.anim.anticipate);
        animAnticipateOvershoot = AnimationUtils.loadAnimation(this, R.anim.anticipate_overshoot);
        animOvershoot = AnimationUtils.loadAnimation(this, R.anim.overshoot);
        animBounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        animCycle = AnimationUtils.loadAnimation(this, R.anim.cycle);
        animLinear = AnimationUtils.loadAnimation(this, R.anim.linear);

        findViewById(R.id.accelerate).setOnClickListener(v -> imageView.startAnimation(animAccelerate));
        findViewById(R.id.decelerate).setOnClickListener(v -> imageView.startAnimation(animDecelerate));
        findViewById(R.id.accelerate_decelerate).setOnClickListener(v -> imageView.startAnimation(animAccelerateDecelerate));
        findViewById(R.id.anticipate).setOnClickListener(v -> imageView.startAnimation(animAnticipate));
        findViewById(R.id.anticipate_overshoot).setOnClickListener(v -> imageView.startAnimation(animAnticipateOvershoot));
        findViewById(R.id.overshoot).setOnClickListener(v -> imageView.startAnimation(animOvershoot));
        findViewById(R.id.bounce).setOnClickListener(v -> imageView.startAnimation(animBounce));
        findViewById(R.id.cycle).setOnClickListener(v -> imageView.startAnimation(animCycle));
        findViewById(R.id.linear).setOnClickListener(v -> imageView.startAnimation(animLinear));
    }
}