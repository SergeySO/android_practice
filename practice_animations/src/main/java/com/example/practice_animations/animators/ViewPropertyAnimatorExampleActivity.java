package com.example.practice_animations.animators;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class ViewPropertyAnimatorExampleActivity extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_property_animator_example);
        imageView = findViewById(R.id.image_view);
        animateFromAnimate(imageView);
    }

    private void animateFromAnimate(ImageView imageView) {
        imageView.animate()
                .setDuration(2000)
                .scaleX(0.5f)
                .scaleY(0.5f)
                .x(400f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        imageView.animate().x(-400f);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
    }
}