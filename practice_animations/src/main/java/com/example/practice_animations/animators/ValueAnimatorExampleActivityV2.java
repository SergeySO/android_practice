package com.example.practice_animations.animators;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class ValueAnimatorExampleActivityV2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_value_animator_example_v2);
        ImageView imageView = findViewById(R.id.image_view);
        animateImage(imageView);
    }

    private void animateImage(ImageView imageView) {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0.7f, 1.f);
        valueAnimator.setDuration(4000);
        valueAnimator.setRepeatMode(ValueAnimator.REVERSE);
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        Interpolator interpolator = new AnticipateInterpolator();
        valueAnimator.setInterpolator(interpolator);

        valueAnimator.addUpdateListener(animator -> {
            float scale = (float) animator.getAnimatedValue();
            imageView.setScaleX(scale);
            imageView.setScaleY(scale);
        });

        valueAnimator.start();
    }
}