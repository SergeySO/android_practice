package com.example.practice_animations.animators;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class AnimatePropertyExampleActivity extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animate_property_example);
        imageView = findViewById(R.id.image_view);
        animatePropertyImage(imageView);
    }

    private void animatePropertyImage(ImageView imageView) {
        PropertyValuesHolder alphaHolder = PropertyValuesHolder.ofFloat("alpha", 0f, 1f);
        PropertyValuesHolder scaleHolder = PropertyValuesHolder.ofFloat("scale", 0.5f, 1f);

        ValueAnimator valueAnimator = ValueAnimator.ofPropertyValuesHolder(alphaHolder, scaleHolder);
        valueAnimator.setDuration(4000);
        valueAnimator.setRepeatMode(ValueAnimator.REVERSE);
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setInterpolator(new AnticipateOvershootInterpolator());
        valueAnimator.addUpdateListener(animation -> {
            float alpha = (float) animation.getAnimatedValue("alpha");
            float scale = (float) animation.getAnimatedValue("scale");
            imageView.setAlpha(alpha);
            imageView.setScaleY(scale);
            imageView.setScaleY(scale);
        });
        valueAnimator.start();
    }
}