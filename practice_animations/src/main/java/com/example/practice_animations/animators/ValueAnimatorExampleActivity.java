package com.example.practice_animations.animators;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class ValueAnimatorExampleActivity extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_value_animator_example);
        imageView = findViewById(R.id.image_view);
        ValueAnimator valueAnimator = (ValueAnimator) AnimatorInflater
                .loadAnimator(this, R.animator.value_animator_example);
        valueAnimator.addUpdateListener(animator ->
                imageView.setAlpha((Float) animator.getAnimatedValue()));
        valueAnimator.start();
    }
}