package com.example.practice_animations.animators;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.practice_animations.R;

public class ObjectAnimatorExampleActivity extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_animator_example);
        imageView = findViewById(R.id.image_view);
        animateFromObjectAnimator(imageView);
    }

    private void animateFromObjectAnimator(ImageView imageView) {
        ObjectAnimator rotationAnimator = ObjectAnimator.ofFloat(imageView, View.ROTATION, 0f, 360f);
        rotationAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        rotationAnimator.setRepeatMode(ObjectAnimator.REVERSE);
        rotationAnimator.setDuration(1000L);
        rotationAnimator.start();
        ObjectAnimator translationAnimator = ObjectAnimator.ofFloat(imageView, View.TRANSLATION_Y, 0f, 300f);
        translationAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        translationAnimator.setRepeatMode(ObjectAnimator.REVERSE);
        translationAnimator.setDuration(1000);
        translationAnimator.start();
    }
}