package ru.sber.android.ptactice_patterns.mvp_v2.presentation.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import ru.sber.android.ptactice_patterns.databinding.ActivityMainBinding;
import ru.sber.android.ptactice_patterns.mvp_v2.data.model.InstalledPackageModel;
import ru.sber.android.ptactice_patterns.mvp_v2.data.provider.PackageInstalledProvider;
import ru.sber.android.ptactice_patterns.mvp_v2.data.repository.IPackageInstalledRepository;
import ru.sber.android.ptactice_patterns.mvp_v2.data.repository.PackageInstalledRepository;
import ru.sber.android.ptactice_patterns.mvp_v2.presentation.presenter.PackageInstalledPresenter;
import ru.sber.android.ptactice_patterns.mvp_v2.presentation.view.adapter.PackageInstalledRecyclerAdapter;
import ru.sber.android.ptactice_patterns.mvp_v2.utils.ISchedulersProvider;
import ru.sber.android.ptactice_patterns.mvp_v2.utils.SchedulersProvider;

/**
 * Главное активити приложения. Умеет показывать список установленных приложений на телефоне.
 *
 */
public class PackageInstalledActivity extends AppCompatActivity implements IPackageInstalledView {

    private PackageInstalledPresenter mMainPresenter;

    private ActivityMainBinding mBinding;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        providePresenter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onStart() {
        super.onStart();

        mMainPresenter.loadDataAsyncRx(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        mMainPresenter.detachView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void showProgress() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void hideProgress() {
        mBinding.progressBar.setVisibility(View.GONE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void showData(@NonNull List<InstalledPackageModel> modelList) {
        PackageInstalledRecyclerAdapter adapter =
                new PackageInstalledRecyclerAdapter(modelList);

        mBinding.recyclerView.setAdapter(adapter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void showError(@NonNull Throwable throwable) {
        Snackbar.make(mBinding.getRoot(), throwable.toString(), BaseTransientBottomBar.LENGTH_LONG).show();
    }

    private void providePresenter() {
        PackageInstalledProvider packageInstalledProvider = new PackageInstalledProvider(this);
        IPackageInstalledRepository packageInstalledRepository =
                new PackageInstalledRepository(packageInstalledProvider);
        ISchedulersProvider schedulersProvider = new SchedulersProvider();

        mMainPresenter = new PackageInstalledPresenter(this, packageInstalledRepository, schedulersProvider);
    }
}
