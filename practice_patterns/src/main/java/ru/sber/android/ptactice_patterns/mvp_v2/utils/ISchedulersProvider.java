package ru.sber.android.ptactice_patterns.mvp_v2.utils;


import io.reactivex.rxjava3.core.Scheduler;

public interface ISchedulersProvider {

    Scheduler io();

    Scheduler ui();
}
