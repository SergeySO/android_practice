package ru.sber.android.ptactice_patterns.mvp_v2.presentation.view;

import androidx.annotation.NonNull;

import java.util.List;

import ru.sber.android.ptactice_patterns.mvp_v2.data.model.InstalledPackageModel;

/**
 * Интерфейс, описывающий возможности View.
 *
 */
public interface IPackageInstalledView {

    /**
     * Показать ProgressBar.
     */
    void showProgress();

    /**
     * Скрыть ProgressBar.
     */
    void hideProgress();

    /**
     * Отобразить данные об установленных приложениях.
     *
     * @param modelList список приложений.
     */
    void showData(@NonNull List<InstalledPackageModel> modelList);

    /**
     * Отобразить ошибку при получении данных.
     *
     * @param throwable возникшая ошибка при получении данных.
     */
    void showError(@NonNull Throwable throwable);
}
