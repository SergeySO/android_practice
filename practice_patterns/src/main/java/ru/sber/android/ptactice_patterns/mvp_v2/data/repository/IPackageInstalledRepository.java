package ru.sber.android.ptactice_patterns.mvp_v2.data.repository;

import androidx.annotation.NonNull;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import ru.sber.android.ptactice_patterns.mvp_v2.data.model.InstalledPackageModel;

/**
 * Интерфейс репозитория по загрузке данных об установленных в системе пакетах.
 *
 */
public interface IPackageInstalledRepository {

    /**
     * Метод для синхронной загрузки данных об установленных в системе приложениях.
     *
     * @param isSystem {@code true} если необходимо показывать системные приложения, {@code false} иначе.
     */
    List<InstalledPackageModel> loadDataSync(boolean isSystem);

    /**
     * Метод для асинхронной загрузки данных об установленных в системе приложениях.
     *
     * @param isSystem {@code true} если необходимо показывать системные приложения, {@code false} иначе.
     * @return Single со списком моделей {@link InstalledPackageModel}.
     */
    Single<List<InstalledPackageModel>> loadDataAsyncRx(boolean isSystem);

    /**
     * Метод для асинхронной загрузки данных об установленных в системе приложениях.
     *
     * @param isSystem                {@code true} если необходимо показывать системные приложения, {@code false} иначе.
     * @param onLoadingFinishListener {@link PackageInstalledRepository.OnLoadingFinishListener} слушатель окончания загрузки.
     */
    void loadDataAsync(boolean isSystem, @NonNull PackageInstalledRepository.OnLoadingFinishListener onLoadingFinishListener);
}
