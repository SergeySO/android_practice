package ru.sber.android.ptactice_patterns.mvp_v2.presentation.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import java.util.List;

import ru.sber.android.ptactice_patterns.R;
import ru.sber.android.ptactice_patterns.databinding.PackageInstalledViewItemBinding;
import ru.sber.android.ptactice_patterns.mvp_v2.data.model.InstalledPackageModel;

/**
 * Адаптер для отображения элементов списка.
 *
 */
public class PackageInstalledRecyclerAdapter extends Adapter<PackageInstalledRecyclerAdapter.PackageInstalledViewHolder> {

    private final List<InstalledPackageModel> mInstalledPackageModelList;

    public PackageInstalledRecyclerAdapter(@NonNull List<InstalledPackageModel> installedPackageModelList) {
        mInstalledPackageModelList = installedPackageModelList;
    }

    @NonNull
    @Override
    public PackageInstalledViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PackageInstalledViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.package_installed_view_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PackageInstalledViewHolder holder, int position) {
        holder.bindView(mInstalledPackageModelList.get(position));
    }

    @Override
    public int getItemCount() {
        return mInstalledPackageModelList.size();
    }

    static class PackageInstalledViewHolder extends RecyclerView.ViewHolder {

        private final PackageInstalledViewItemBinding mBinding;

        PackageInstalledViewHolder(@NonNull View itemView) {
            super(itemView);

            mBinding = PackageInstalledViewItemBinding.bind(itemView);
        }

        void bindView(@NonNull InstalledPackageModel installedPackageModel) {
            mBinding.appNameTextView.setText(installedPackageModel.getAppName());
            mBinding.appPackageTextView.setText(installedPackageModel.getAppPackageName());
            mBinding.appIconImageView.setImageDrawable(installedPackageModel.getAppIcon());
        }
    }
}
