package ru.sber.android.ptactice_patterns.mvp.presentation;

import java.util.List;

import ru.sber.android.ptactice_patterns.common.User;
import ru.sber.android.ptactice_patterns.mvp.data.UserData;

public interface UsersContractView {
    UserData getUserData();
    void showUsers(List<User> users);
    void showToast(int resId);
    void showProgress();
    void hideProgress();
}
