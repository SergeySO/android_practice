package com.example.practike_touch_event.velocity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;

import com.example.practike_touch_event.R;

import static android.view.MotionEvent.ACTION_UP;

public class VelocityExampleActivity extends AppCompatActivity {
    public static final String TAG = "Velocity";
    private VelocityTracker myVelocityTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_velocity);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        obtainVelocityTracker(event);

        switch (action) {
            case ACTION_UP:
                final VelocityTracker velocityTracker = myVelocityTracker;
                //Determine the pointer’s velocity//
                velocityTracker.computeCurrentVelocity(1000);
                //Retrieve the velocity for each pointer//
                float xVelocity = myVelocityTracker.getXVelocity();
                float yVelocity = myVelocityTracker.getYVelocity();
                //Log the velocity in pixels per second//
                Log.i(TAG, "xVelocity: " + xVelocity + ", yVelocity: " + yVelocity);
                //Reset the velocity tracker to its initial state, ready to record the next gesture//
                myVelocityTracker.clear();
                break;
            default:
                break;
        }

        return true;
    }

    private void obtainVelocityTracker(MotionEvent event) {
        if (myVelocityTracker == null) {
            myVelocityTracker = VelocityTracker.obtain();
        }

        myVelocityTracker.addMovement(event);
    }
}