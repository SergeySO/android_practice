package com.example.practike_touch_event.shapes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.practike_touch_event.Box;

import java.util.ArrayList;
import java.util.List;

import static android.view.MotionEvent.ACTION_CANCEL;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;

public class DrawLineView extends View {
    private final float STROKE_WIDTH = 10f;

    private final List<Box> mBoxes = new ArrayList<>();
    private final Paint mPaint = new Paint();
    private Box mCurrentBox;

    public DrawLineView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setupPaint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for(Box lineBox : mBoxes) {
            float startX = lineBox.getOrigin().x;
            float startY = lineBox.getOrigin().y;
            float stopX = lineBox.getCurrent().x;
            float stopY = lineBox.getCurrent().y;
            canvas.drawLine(startX, startY, stopX, stopY, mPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        PointF currentPoint = new PointF(event.getX(), event.getY());
        int action = event.getAction();

        switch (action) {
            case ACTION_DOWN:
                mCurrentBox = new Box(currentPoint);
                mBoxes.add(mCurrentBox);
                break;
            case ACTION_MOVE:
                if (mCurrentBox != null) {
                    mCurrentBox.setCurrent(currentPoint);
                    invalidate();
                }
                break;
            case ACTION_UP:
            case ACTION_CANCEL:
                mCurrentBox = null;
                break;
        }

        return true;
    }

    public void reset() {
        mBoxes.clear();
        invalidate();
    }

    private void setupPaint() {
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(STROKE_WIDTH);
    }
}
