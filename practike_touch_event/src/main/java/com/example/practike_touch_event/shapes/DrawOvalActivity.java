package com.example.practike_touch_event.shapes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import com.example.practike_touch_event.R;
import com.example.practike_touch_event.shapes.DrawOvalView;

public class DrawOvalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_oval);
        DrawOvalView drawOvalView = findViewById(R.id.draw_view);
        Button btnReset = findViewById(R.id.btn_reset);
        btnReset.setOnClickListener(v -> drawOvalView.reset());
    }
}