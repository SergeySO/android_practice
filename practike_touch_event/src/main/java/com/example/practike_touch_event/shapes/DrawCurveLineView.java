package com.example.practike_touch_event.shapes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import static android.view.MotionEvent.*;

public class DrawCurveLineView extends View {
    private static final float STROKE_WIDTH = 10f;

    private Paint mPaint = new Paint();
    private Path mPath = new Path();

    public DrawCurveLineView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setupPaint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        PointF currentPoint = new PointF(event.getX(), event.getY());
        int action = event.getAction();

        switch (action) {
            case ACTION_DOWN:
                mPath.moveTo(currentPoint.x, currentPoint.y);
                break;
            case ACTION_MOVE:
                mPath.lineTo(currentPoint.x, currentPoint.y);
                invalidate();
                break;
            case ACTION_UP:
            case ACTION_CANCEL:
                break;
        }

        return true;
    }

    public void reset() {
        mPath.reset();
        invalidate();
    }

    private void setupPaint() {
        mPaint.setColor(Color.GREEN);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(STROKE_WIDTH);
    }
}
