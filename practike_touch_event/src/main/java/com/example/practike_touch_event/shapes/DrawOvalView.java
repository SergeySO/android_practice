package com.example.practike_touch_event.shapes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.practike_touch_event.Box;

import java.util.ArrayList;
import java.util.List;

import static android.view.MotionEvent.ACTION_CANCEL;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;

public class DrawOvalView extends View {
    private static final float STROKE_WIDTH = 10f;

    private final List<Box> mBoxes =  new ArrayList<>();
    private final Paint mPaint = new Paint();
    private Box currentBox;

    public DrawOvalView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setupPaint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (Box box : mBoxes) {
            float left = Math.min(box.getOrigin().x, box.getCurrent().x);
            float right = Math.max(box.getOrigin().x, box.getCurrent().x);
            float top = Math.min(box.getOrigin().y, box.getCurrent().y);
            float bottom = Math.max(box.getOrigin().y, box.getCurrent().y);
            canvas.drawOval(left, top, right, bottom, mPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        PointF currentPoint = new PointF(event.getX(), event.getY());
        int action = event.getAction();

        switch (action) {
            case ACTION_DOWN:
                currentBox = new Box(currentPoint);
                mBoxes.add(currentBox);
                break;
            case ACTION_MOVE:
                if (currentBox != null) {
                    currentBox.setCurrent(currentPoint);
                    invalidate();
                }
                break;
            case ACTION_UP:
            case ACTION_CANCEL:
                break;
        }

        return true;
    }

    public void reset() {
        mBoxes.clear();
        invalidate();
    }

    private void setupPaint() {
        mPaint.setColor(Color.YELLOW);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(STROKE_WIDTH);
    }
}
