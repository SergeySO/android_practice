package com.example.practike_touch_event.shapes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import com.example.practike_touch_event.R;
import com.example.practike_touch_event.shapes.DrawRectView;

public class DrawRectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_rect);
        DrawRectView drawRectView = findViewById(R.id.draw_view);
        Button btnReset = findViewById(R.id.btn_reset);
        btnReset.setOnClickListener(v -> drawRectView.reset());
    }
}