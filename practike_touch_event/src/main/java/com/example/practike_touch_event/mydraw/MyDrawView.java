package com.example.practike_touch_event.mydraw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.practike_touch_event.Box;

import java.util.ArrayList;
import java.util.List;

import static android.view.MotionEvent.ACTION_CANCEL;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;

public class MyDrawView extends View {
    private static final float STROKE_WIDTH = 10f;

    private final List<Box> mCurveLineBoxes = new ArrayList<>();
    private final List<Box> mLineBoxes = new ArrayList<>();
    private final List<Box> mOvalBoxes = new ArrayList<>();
    private final List<Box> mRectBoxes = new ArrayList<>();
    private final Paint mPaint = new Paint();
    private Box mCurrentBox;
    private Shape mCurrentShape = Shape.RECT;
    private int mCurrentColor = Color.BLACK;

    public MyDrawView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setupPaint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawRect(canvas);
        drawOval(canvas);
        drawLine(canvas);
        drawCurveLine(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        PointF currentPoint = new PointF(event.getX(), event.getY());
        int action = event.getAction();

        switch (action) {
            case ACTION_DOWN:
                if (Shape.CURVE == mCurrentShape) {
                    //todo не уверен в правильности т.к. каждый раз создаем объект Path (Сделал для того, чтобы линии были разных цветов).
                    mCurrentBox = new Box(currentPoint, mCurrentColor, new Path());
                } else {
                    mCurrentBox = new Box(currentPoint, mCurrentColor);
                }

                if (Shape.RECT == mCurrentShape) {
                    mRectBoxes.add(mCurrentBox);
                } else if (Shape.OVAL == mCurrentShape) {
                    mOvalBoxes.add(mCurrentBox);
                } else if(Shape.LINE == mCurrentShape) {
                    mLineBoxes.add(mCurrentBox);
                } else {
                    mCurrentBox.getPath().moveTo(mCurrentBox.getOrigin().x, mCurrentBox.getOrigin().y);
                    mCurveLineBoxes.add(mCurrentBox);
                }

                break;
            case ACTION_MOVE:
                if (mCurrentBox != null) {
                    if (Shape.RECT == mCurrentShape) {
                        mCurrentBox.setCurrent(currentPoint);
                    } else if (Shape.OVAL == mCurrentShape) {
                        mCurrentBox.setCurrent(currentPoint);
                    } else if(Shape.LINE == mCurrentShape) {
                        mCurrentBox.setCurrent(currentPoint);
                    } else {
                        mCurrentBox.setCurrent(currentPoint);
                        mCurrentBox.getPath().lineTo(currentPoint.x, currentPoint.y);
                    }

                    invalidate();
                }

                break;
            case ACTION_UP:
            case ACTION_CANCEL:
                mCurrentBox = null;
                break;
        }

        return true;
    }

    public void reset() {
        mLineBoxes.clear();
        mOvalBoxes.clear();
        mRectBoxes.clear();
        mCurveLineBoxes.clear();
        invalidate();
    }

    public void setCurrentShape(Shape mCurrentShape) {
        this.mCurrentShape = mCurrentShape;
    }

    private void drawRect(Canvas canvas) {
        for (Box box : mRectBoxes) {
            float left = Math.min(box.getOrigin().x, box.getCurrent().x);
            float right = Math.max(box.getOrigin().x, box.getCurrent().x);
            float top = Math.min(box.getOrigin().y, box.getCurrent().y);
            float bottom = Math.max(box.getOrigin().y, box.getCurrent().y);
            mPaint.setColor(box.getColor());
            canvas.drawRect(left, top, right, bottom, mPaint);
        }
    }

    private void drawOval(Canvas canvas) {
        for (Box ovalBox : mOvalBoxes) {
            float left = Math.min(ovalBox.getOrigin().x, ovalBox.getCurrent().x);
            float right = Math.max(ovalBox.getOrigin().x, ovalBox.getCurrent().x);
            float top = Math.min(ovalBox.getOrigin().y, ovalBox.getCurrent().y);
            float bottom = Math.max(ovalBox.getOrigin().y, ovalBox.getCurrent().y);
            mPaint.setColor(ovalBox.getColor());
            canvas.drawOval(left, top, right, bottom, mPaint);
        }
    }

    private void drawLine(Canvas canvas) {
        for (Box lineBox : mLineBoxes) {
            float startX = lineBox.getOrigin().x;
            float startY = lineBox.getOrigin().y;
            float stopX = lineBox.getCurrent().x;
            float stopY = lineBox.getCurrent().y;
            mPaint.setColor(lineBox.getColor());
            canvas.drawLine(startX, startY, stopX, stopY, mPaint);
        }
    }

    private void drawCurveLine(Canvas canvas) {
        for (Box curveLineBox : mCurveLineBoxes) {
            mPaint.setColor(curveLineBox.getColor());
            canvas.drawPath(curveLineBox.getPath(), mPaint);
        }
    }

    public void setColor(int color) {
        this.mCurrentColor = color;
    }

    private void setupPaint() {
        mPaint.setStrokeWidth(STROKE_WIDTH);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLACK);
    }
}
