package com.example.practike_touch_event.mydraw;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.practike_touch_event.R;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import com.jaredrummler.android.colorpicker.ColorShape;

public class  MyDrawActivity extends AppCompatActivity implements ColorPickerDialogListener {
    private static final int btnColorId = 1;
    private MyDrawView drawView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_draw_activity_main);
        init();
    }

    public void init() {
        drawView = findViewById(R.id.draw_view);
        Button btnReset = findViewById(R.id.btn_reset);
        Button btnRect = findViewById(R.id.btn_rect);
        Button btnOval = findViewById(R.id.btn_oval);
        Button btnLine = findViewById(R.id.btn_line);
        Button btnCurve = findViewById(R.id.btn_curve);
        btnReset.setOnClickListener(v -> drawView.reset());
        btnRect.setOnClickListener(v -> drawView.setCurrentShape(Shape.RECT));
        btnOval.setOnClickListener(v -> drawView.setCurrentShape(Shape.OVAL));
        btnLine.setOnClickListener(v -> drawView.setCurrentShape(Shape.LINE));
        btnCurve.setOnClickListener(v -> drawView.setCurrentShape(Shape.CURVE));
    }

    private void createColorPickerDialog() {
        ColorPickerDialog.newBuilder()
                .setColor(Color.RED)
                .setDialogType(ColorPickerDialog.TYPE_PRESETS)
                .setAllowCustom(true)
                .setAllowPresets(true)
                .setColorShape(ColorShape.SQUARE)
                .setDialogId(MyDrawActivity.btnColorId)
                .show(this);
    }

    public void onClickButton(View view) {
        if (view.getId() == R.id.btn_color) {
            createColorPickerDialog();
        }
    }

    @Override
    public void onColorSelected(int dialogId, int color) {
        if (dialogId == btnColorId) {
            drawView.setColor(color);
        }
    }

    @Override
    public void onDialogDismissed(int dialogId) {
        Toast.makeText(this, "Dialog dismissed", Toast.LENGTH_SHORT).show();
    }
}