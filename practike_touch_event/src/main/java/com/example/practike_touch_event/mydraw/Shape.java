package com.example.practike_touch_event.mydraw;

public enum Shape {
    RECT,
    OVAL,
    LINE,
    CURVE
}
