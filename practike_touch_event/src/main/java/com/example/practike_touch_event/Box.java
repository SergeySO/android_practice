package com.example.practike_touch_event;

import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PointF;

public class Box {
    private Path path;
    private PointF mOrigin;
    private PointF mCurrent;
    private int mColor;

    public Box(PointF mOrigin) {
        this(mOrigin, Color.BLACK);
    }

    public Box(PointF mOrigin, int color) {
        this.mOrigin = mOrigin;
        this.mCurrent = mOrigin;
        this.mColor = color;
    }

    public Box(PointF mOrigin, int color, Path path) {
        this.mOrigin = mOrigin;
        this.mCurrent = mOrigin;
        this.mColor = color;
        this.path = path;
    }

    public PointF getOrigin() {
        return mOrigin;
    }

    public void setOrigin(PointF mOrigin) {
        this.mOrigin = mOrigin;
    }

    public PointF getCurrent() {
        return mCurrent;
    }

    public void setCurrent(PointF mCurrent) {
        this.mCurrent = mCurrent;
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int mColor) {
        this.mColor = mColor;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }
}
