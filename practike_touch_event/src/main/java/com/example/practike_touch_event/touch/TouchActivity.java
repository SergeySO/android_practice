package com.example.practike_touch_event.touch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

import com.example.practike_touch_event.R;

import static android.view.MotionEvent.ACTION_CANCEL;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;

public class TouchActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case ACTION_DOWN:
                Log.i(TAG, "Down action");
                return true;
            case ACTION_MOVE:
                Log.i(TAG, "Move action");
                return true;
            case ACTION_UP:
                Log.i(TAG, "Up action");
                return true;
            case ACTION_CANCEL:
                Log.i(TAG, "Cancel action");
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }
}