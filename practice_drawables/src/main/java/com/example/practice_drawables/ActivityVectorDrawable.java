package com.example.practice_drawables;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class ActivityVectorDrawable extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vector_drawable);
    }
}