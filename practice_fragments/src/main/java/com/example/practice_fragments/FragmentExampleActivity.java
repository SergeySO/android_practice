package com.example.practice_fragments;

import androidx.appcompat.app.AppCompatActivity;
import com.example.practice_fragments.fragments.ButtonFragment;
import com.example.practice_fragments.fragments.CheckBoxFragment;
import com.example.practice_fragments.fragments.ImageFragment;

import android.os.Bundle;

public class FragmentExampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_example);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.button_fragment, ButtonFragment.newInstance())
                .add(R.id.checkbox_fragment, CheckBoxFragment.newInstance())
                .add(R.id.image_fragment, ImageFragment.newInstance())
                .commit();
    }
}