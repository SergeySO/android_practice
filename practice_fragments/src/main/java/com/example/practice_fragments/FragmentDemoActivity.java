package com.example.practice_fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

import com.example.practice_fragments.fragments.BlueFragment;
import com.example.practice_fragments.fragments.RedFragment;

public class FragmentDemoActivity extends AppCompatActivity {
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_demo);
        mFragmentManager = getSupportFragmentManager();
    }

    public void onClick(View view) {
        mFragmentTransaction = mFragmentManager.beginTransaction();

        switch (view.getId()) {
            case R.id.button_1:
                mFragmentTransaction.add(R.id.my_container, BlueFragment.newInstance());
                break;
            case R.id.button_2:
                mFragmentTransaction.add(R.id.my_container, RedFragment.newInstance());
                break;
        }

        mFragmentTransaction.commit();
    }
}