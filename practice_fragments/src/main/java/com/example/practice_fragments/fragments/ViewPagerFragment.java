package com.example.practice_fragments.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.practice_fragments.R;

public class ViewPagerFragment extends Fragment {
    private static final String TAG = "ViewPagerFragment";
    public static final String STRING_KEY = "STRING_KEY";
    private TextView textView;
    private int position;

    public static ViewPagerFragment newInstance(int position) {

        Bundle bundle = new Bundle();
        bundle.putInt(STRING_KEY, position);
        ViewPagerFragment viewPagerFragment = new ViewPagerFragment();
        viewPagerFragment.setArguments(bundle);
        return viewPagerFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable  Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: " + position);
        return inflater.inflate(R.layout.third_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: " + position);
        textView = view.findViewById(R.id.text_view);
        textView.setText(String.valueOf(position));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        position = getArguments().getInt(STRING_KEY);
        Log.d(TAG, "onAttach: " + position);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: " + position);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: " + position);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: " + position);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: " + position);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: " + position);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: " + position);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView: " + position);
    }
}
