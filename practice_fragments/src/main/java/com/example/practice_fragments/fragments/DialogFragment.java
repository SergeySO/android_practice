package com.example.practice_fragments.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.practice_fragments.R;

public class DialogFragment extends Fragment {
    private Button adapterButton;

    public static DialogFragment newInstance() {
        return new DialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        adapterButton = view.findViewById(R.id.adapter_button);
        adapterButton.setOnClickListener(view12 -> new AlertDialog.Builder(view12.getContext())
                .setTitle("Title")
                .setMessage("Message")
                .setPositiveButton("Positive", (dialogInterface, i) -> dialogInterface.dismiss())
                .setNegativeButton("Negative", (dialogInterface, i) -> dialogInterface.dismiss())
                .setNeutralButton("Neutral", (dialogInterface, i) -> dialogInterface.dismiss())
                .setCancelable(false)
                .show());
    }
}
