package com.example.practice_fragments.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.practice_fragments.fragments.ViewPagerFragment;

public class MyAdapter extends FragmentStateAdapter {
    private final int count;

    public MyAdapter(@NonNull FragmentActivity fragmentActivity, int count) {
        super(fragmentActivity);
        this.count = count;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return ViewPagerFragment.newInstance(position);
    }

    @Override
    public int getItemCount() {
        return count;
    }
}
