package com.example.practice_fragments;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.practice_fragments.fragments.ContentFragment;

public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.content_fragment, ContentFragment.newInstance())
                .commit();
    }
}