package com.example.practice_fragments;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.practice_fragments.fragments.DialogFragment;

public class AlertDialogExampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_dialog_example);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.alert_dialog, DialogFragment.newInstance())
                .commit();
    }
}