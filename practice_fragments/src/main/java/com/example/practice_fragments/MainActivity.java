package com.example.practice_fragments;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.practice_fragments.fragments.FirstFragment;
import com.example.practice_fragments.fragments.SecondFragment;
import com.example.practice_fragments.fragments.ThirdFragment;

public class MainActivity extends AppCompatActivity implements SecondFragment.CallBack, FirstFragment.CallBack1 {
    private String text;
    private ViewGroup thirdContainer;
    private static  final String FRAGMENT_TAG = "FRAGMENT_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.first_fragment, FirstFragment.newInstance())
                .add(R.id.second_fragment, SecondFragment.newInstance())
                .commit();
    }

    @Override
    public void createThirdFragment() {
        createFragment();
        Fragment f3 = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);

        if (f3 == null) {
            f3 = ThirdFragment.newInstance(text);
        } else {
            ((ThirdFragment) f3).setText(text);
        }

        //todo  java.lang.IllegalStateException: Can't change container ID of fragment ThirdFragment{fa7c95f} (03955ff7-6c69-4fec-b1cb-e65cbdc5994f id=0x1 tag=FRAGMENT_TAG): was 1 now 2
        getSupportFragmentManager().beginTransaction()
                .replace(thirdContainer.getId(), f3, FRAGMENT_TAG)
                .commit();
    }

    public void createFragment() {
        ViewGroup root = findViewById(R.id.root);

        if (thirdContainer == null) {
            thirdContainer = new FragmentContainerView(this);
            thirdContainer.setId(View.generateViewId());
            thirdContainer.setLayoutParams(
                    new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 3));
            root.addView(thirdContainer);
        }
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }
}