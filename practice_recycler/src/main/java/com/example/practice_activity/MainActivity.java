package com.example.practice_activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import com.example.practice_activity.user.User;
import com.example.practice_activity.user.UserAdapter;

public class MainActivity extends AppCompatActivity {
    private final List<User> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        generateData();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_user);
        UserAdapter userAdapter = new UserAdapter(this, users);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(userAdapter);
    }

    private void generateData() {
        users.add(new User(R.drawable.avatar,  "Иванов", "Иван", "Иванович"));
        users.add(new User(R.drawable.avatar,  "Петров", "Иван", "Иванович"));
        users.add(new User(R.drawable.avatar,  "Сидоров", "Иван", "Иванович"));
        users.add(new User(R.drawable.avatar,  "Суд", "Иван", "Иванович"));
        users.add(new User(R.drawable.avatar,  "Бклочка", "Иван", "Иванович"));
    }
}