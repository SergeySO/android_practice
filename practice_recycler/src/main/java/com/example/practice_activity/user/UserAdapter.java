package com.example.practice_activity.user;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.example.practice_activity.R;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder> {
    private final LayoutInflater inflater;
    private final List<User> users;

    public UserAdapter(Context context, List<User> users) {
        this.inflater = LayoutInflater.from(context);
        this.users = users;
    }

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_user, parent, false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        User user = users.get(position);
        holder.avatarView.setImageDrawable(AppCompatResources.getDrawable(inflater.getContext(), user.getAvatar()));
        holder.firstNameView.setText(user.getFirstName());
        holder.lastNameView.setText(user.getLastName());
        holder.middleNameView.setText(user.getMiddleName());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class UserHolder extends RecyclerView.ViewHolder {
        private final ImageView avatarView;
        private final TextView firstNameView;
        private final TextView lastNameView;
        private final TextView middleNameView;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            avatarView = itemView.findViewById(R.id.avatarImageView);
            firstNameView = itemView.findViewById(R.id.firstNameView);
            lastNameView = itemView.findViewById(R.id.lastNameView);
            middleNameView = itemView.findViewById(R.id.middleNameView);
        }
    }
}
