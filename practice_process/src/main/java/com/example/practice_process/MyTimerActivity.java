package com.example.practice_process;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyTimerActivity extends AppCompatActivity {
    private final Handler handler = new Handler(Looper.getMainLooper());
    private EditText editText;
    private TextView textViewTime;
    private Button btnStart;
    private Button btnStop;
    private Runnable runnable;
    private int time = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_timer);
        init();

        btnStart.setOnClickListener(view -> {
            String currentTime = editText.getText().toString();

            if (currentTime.isEmpty()) {
                time = 0;
            } else {
                time = Integer.parseInt(currentTime);
            }

            start(view);
        });
        btnStop.setOnClickListener(this::stop);
    }

    private void init() {
        editText = findViewById(R.id.editViewTime);
        textViewTime = findViewById(R.id.textViewTime);
        btnStart = findViewById(R.id.btnStart);
        btnStop = findViewById(R.id.btnStop);
    }

    private void timer(int time) {
        textViewTime.setText(String.valueOf(time));
    }

    private void start(View view) {
        runnable = () -> {
            if (time == 0) {
                MyTimerActivity.this.stop(view);
            } else {
                time -= 1;
                MyTimerActivity.this.timer(time);
                handler.postDelayed(runnable, 1000);
            }
        };

        handler.post(runnable);
    }

    private void stop(View view) {
        handler.removeCallbacks(runnable);
        time = 0;
        timer(time);
    }
}